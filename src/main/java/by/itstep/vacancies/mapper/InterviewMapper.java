package by.itstep.vacancies.mapper;
import by.itstep.vacancies.dto.*;
import by.itstep.vacancies.entity.InterviewEntity;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface InterviewMapper {

    InterviewFullDto map(InterviewEntity entity);

    List<InterviewFullDto> map(List<InterviewEntity> entities);
}
