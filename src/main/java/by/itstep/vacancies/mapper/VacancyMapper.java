package by.itstep.vacancies.mapper;
import by.itstep.vacancies.dto.*;
import by.itstep.vacancies.entity.InterviewEntity;
import by.itstep.vacancies.entity.VacancyEntity;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface VacancyMapper {

    VacancyFullDto map(VacancyEntity entity);
    VacancyEntity map(VacancyCreateDto dto);
    List<VacancyShortDto> map(List<VacancyEntity> entities);
}
