package by.itstep.vacancies.mapper;
import by.itstep.vacancies.dto.UserCreateDto;
import by.itstep.vacancies.dto.UserFullDto;
import by.itstep.vacancies.dto.UserShortDto;
import by.itstep.vacancies.entity.UserEntity;
import org.mapstruct.Mapper;
import java.util.List;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserFullDto map(UserEntity entity);
    UserEntity map(UserCreateDto dto);
    List<UserShortDto> map(List<UserEntity> entities);
}
