package by.itstep.vacancies.service;
import by.itstep.vacancies.config.SecurityService;
import by.itstep.vacancies.dto.*;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.entity.VacancyEntity;
import by.itstep.vacancies.exceptions.*;
import by.itstep.vacancies.mapper.VacancyMapper;
import by.itstep.vacancies.repository.UserRepository;
import by.itstep.vacancies.repository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class VacancyService {

    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private VacancyMapper vacancyMapper;
    @Autowired
    private SecurityService securityService;

    public VacancyFullDto create(VacancyCreateDto createRequest){
        VacancyEntity vacancy = vacancyMapper.map(createRequest);
        if(vacancy.getId()!=null){
            throw new CreatingVacancyWithIdException("VacancyService -> Can not create vacancy with id.");
        }
        VacancyEntity createdVacancy = vacancyRepository.create(vacancy);
        System.out.println("VacancyService -> Created vacancy: "+ vacancy);
        VacancyFullDto createdDto = vacancyMapper.map(createdVacancy);
        return  createdDto;
    }

    public VacancyFullDto findById(int id){
        VacancyEntity vacancy = vacancyRepository.findById(id);
        if(vacancy==null){
            throw new VacancyDoesNotExistException("VacancyService ->Vacancy not found by id: "+ id);
        }
        System.out.println("VacancyService -> Found vacancy: "+ vacancy);
        VacancyFullDto dto = vacancyMapper.map(vacancy);
        return dto;
    }

    public List<VacancyShortDto> findAll(){
        List<VacancyEntity> allVacancies = vacancyRepository.findAll();
        System.out.println("VacancyService -> Found vacancies: "+ allVacancies);
        List<VacancyShortDto> allDtos = vacancyMapper.map(allVacancies);
        return allDtos;
    }


    public VacancyFullDto update(VacancyUpdateDto updateRequest){
        UserEntity currentUser = securityService.getAuthenticatedUser();
        if(!currentUser.getId().equals(updateRequest.getId())){
            throw new RuntimeException("UserService -> Can not update another user");
        }
        if(updateRequest.getId()==null){
            throw new UpdatingVacancyWithoutIdException("VacancyService -> Can not update entity without id");
        }
        if(vacancyRepository.findById(updateRequest.getId())==null){
            throw new VacancyDoesNotExistException("VacancyService -> Vacancy not found by id:"+ updateRequest.getId());
        }
        VacancyEntity vacancy = vacancyRepository.findById(updateRequest.getId());
        vacancy.setName(updateRequest.getName());
        vacancy.setSalary(updateRequest.getSalary());
        vacancy.setPosition(updateRequest.getPosition());
        vacancy.setCompanyName(updateRequest.getCompanyName());
        VacancyEntity updatedVacancy = vacancyRepository.update(vacancy);
        System.out.println("VacancyService -> Updated vacancy: "+ vacancy);
        VacancyFullDto updatedDto = vacancyMapper.map(updatedVacancy);
        return updatedDto;
    }


    public void deleteById(int id){
        UserEntity currentUser = securityService.getAuthenticatedUser();
        if(!currentUser.getId().equals(id)){
            throw new RuntimeException("UserService -> Can not update another user");
        }
        VacancyEntity vacancyToDelete = vacancyRepository.findById(id);
        if(vacancyToDelete==null){
            throw new VacancyDoesNotExistException("VacancyService -> Vacancy wasn't found by id: "+id+
                    " and can not be deleted.");
        }
        vacancyRepository.deleteById(id);
        System.out.println("VacancyService -> Deleted vacancy: "+ vacancyToDelete);
    }

}
