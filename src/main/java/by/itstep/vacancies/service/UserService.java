package by.itstep.vacancies.service;
import by.itstep.vacancies.config.SecurityService;
import by.itstep.vacancies.dto.UserCreateDto;
import by.itstep.vacancies.dto.UserFullDto;
import by.itstep.vacancies.dto.UserShortDto;
import by.itstep.vacancies.dto.UserUpdateDto;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.exceptions.CreatingUserWithIdException;
import by.itstep.vacancies.exceptions.EmailIsAlreadyTakenException;
import by.itstep.vacancies.exceptions.UpdatingUserWithoutIdException;
import by.itstep.vacancies.exceptions.UserDoesNotExistException;
import by.itstep.vacancies.mapper.UserMapper;
import by.itstep.vacancies.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserMapper userMapper;
    @Autowired
    private SecurityService securityService;


    public UserFullDto create(UserCreateDto createRequest){
        UserEntity user = userMapper.map(createRequest);
        if(user.getId()!=null){
            throw new CreatingUserWithIdException("UserService -> Can not create user with id.");

        }
        List<UserEntity> existingUsers = userRepository.findAll();
        for(UserEntity u: existingUsers){
            if(u.getEmail().equalsIgnoreCase(user.getEmail())){
                throw new EmailIsAlreadyTakenException("UserService -> Email: "+ user.getEmail()+" is taken!");
            }
        }
        UserEntity createdUser = userRepository.create(user);
        System.out.println("UserService -> Created user: "+ user);
        UserFullDto createdDto = userMapper.map(createdUser);
        return  createdDto;
    }


    public UserFullDto findById(int id){
        UserEntity user = userRepository.findById(id);
        if(user==null){
            throw new UserDoesNotExistException("UserService ->User not found by id: "+ id);
        }
        System.out.println("UserService -> Found user: "+ user);
        UserFullDto dto = userMapper.map(user);
        return dto;
    }

    public List<UserShortDto> findAll(){
        List<UserEntity> allUsers = userRepository.findAll();
        System.out.println("UserService -> Found users: "+ allUsers);
        List<UserShortDto> allDtos = userMapper.map(allUsers);
        return allDtos;
    }


    public UserFullDto update(UserUpdateDto updateRequest){
        UserEntity currentUser = securityService.getAuthenticatedUser();
        if(!currentUser.getId().equals(updateRequest.getId())){
            throw new RuntimeException("UserService -> Can not update another user");
        }
        if(updateRequest.getId()==null){
            throw new UpdatingUserWithoutIdException("UserService -> Can not update entity without id");
        }
        if(userRepository.findById(updateRequest.getId())==null){
            throw new UserDoesNotExistException("UserService -> User not found by id:"+ updateRequest.getId());
        }
        UserEntity user = userRepository.findById(updateRequest.getId());
        user.setFirstName(updateRequest.getFirstName());
        user.setLastName(updateRequest.getLastName());
        user.setPhone(updateRequest.getPhone());
        user.setPosition(updateRequest.getPosition());
        user.setYearsOfExperience(updateRequest.getYearsOfExperience());
        UserEntity updatedUser = userRepository.update(user);
        System.out.println("UserService -> Updated user: "+ user);
        UserFullDto updatedDto = userMapper.map(updatedUser);
        return updatedDto;
    }


    public void deleteById(int id){
        UserEntity currentUser = securityService.getAuthenticatedUser();
        if(!currentUser.getId().equals(id)){
            throw new RuntimeException("UserService -> Can not update another user");
        }
        UserEntity userToDelete = userRepository.findById(id);
        if(userToDelete==null){
            throw new UserDoesNotExistException("UserService -> User wasn't found by id: "+id+
                    " and can not be deleted.");
        }
        userRepository.deleteById(id);
        System.out.println("UserService -> Deleted user: "+ userToDelete);
    }

}


