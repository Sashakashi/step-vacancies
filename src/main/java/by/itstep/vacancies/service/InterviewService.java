package by.itstep.vacancies.service;
import by.itstep.vacancies.config.SecurityService;
import by.itstep.vacancies.dto.InterviewCreateDto;
import by.itstep.vacancies.dto.InterviewFullDto;
import by.itstep.vacancies.dto.InterviewUpdateDto;
import by.itstep.vacancies.entity.InterviewEntity;
import by.itstep.vacancies.entity.InterviewStatus;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.entity.VacancyEntity;
import by.itstep.vacancies.exceptions.*;
import by.itstep.vacancies.mapper.InterviewMapper;
import by.itstep.vacancies.repository.InterviewRepository;
import by.itstep.vacancies.repository.UserRepository;
import by.itstep.vacancies.repository.VacancyRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
public class InterviewService {

    @Autowired
    private InterviewRepository interviewRepository;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;
    @Autowired
    private InterviewMapper interviewMapper;
    @Autowired
    private SecurityService securityService;



    public InterviewFullDto create(InterviewCreateDto createRequest){
        UserEntity user = userRepository.findById(createRequest.getUserId());
        VacancyEntity vacancy = vacancyRepository.findById(createRequest.getVacancyId());
        InterviewEntity interviewToCreate = new InterviewEntity();
        interviewToCreate.setUser(user);
        interviewToCreate.setVacancy(vacancy);
        interviewToCreate.setDate(LocalDate.now());
        interviewToCreate.setStatus(InterviewStatus.NEW);
        List<InterviewEntity> existingInterviews = interviewRepository.findAll();
        for(InterviewEntity i: existingInterviews){
            if(i.getUser().getId().equals(interviewToCreate.getUser().getId())&&
                    i.getVacancy().getId().equals(interviewToCreate.getVacancy().getId())){
                throw new CreatingInterviewAlreadyExistsException("InterviewService -> User: "+ interviewToCreate.getUser()+
                        " is already appointed for interview. Vacancy: "+interviewToCreate.getVacancy());
            }
        }
        InterviewEntity createdInterview = interviewRepository.create(interviewToCreate);
        System.out.println("InterviewService -> Created interview: "+ createdInterview);
        InterviewFullDto dto = interviewMapper.map(createdInterview);
        return  dto;
    }


    public InterviewFullDto findById(int id){
        InterviewEntity interview = interviewRepository.findById(id);
        if(interview==null){
            throw new InterviewDoesNotExistException("InterviewService ->Interview not found by id: "+ id);
        }
        System.out.println("InterviewService -> Found interview: "+ interview);
        InterviewFullDto dto = interviewMapper.map(interview);
        return dto;
    }

    public List<InterviewFullDto> findAll(){
        List<InterviewEntity> allInterviews = interviewRepository.findAll();
        System.out.println("InterviewService -> Found interviews: "+ allInterviews);
        List<InterviewFullDto> allDtos = interviewMapper.map(allInterviews);
        return allDtos;
    }


    public InterviewFullDto updateStatus(InterviewUpdateDto updateRequest){
        UserEntity currentUser = securityService.getAuthenticatedUser();
        if(!currentUser.getId().equals(updateRequest.getId())){
            throw new RuntimeException("UserService -> Can not update another user");
        }
        if(updateRequest.getId()==null){
            throw new UpdatingInterviewWithoutIdException("InterviewService -> Can not update entity without id");
        }
        InterviewEntity interviewToUpdate = interviewRepository.findById(updateRequest.getId());
        interviewToUpdate.setStatus(updateRequest.getStatus());
        InterviewEntity updatedInterview = interviewRepository.update(interviewToUpdate);
        System.out.println("InterviewService -> Updated interview: "+ updatedInterview);
        InterviewFullDto updatedDto = interviewMapper.map(updatedInterview);
        return updatedDto;
    }


    public void deleteById(int id){
        UserEntity currentUser = securityService.getAuthenticatedUser();
        if(!currentUser.getId().equals(id)){
            throw new RuntimeException("UserService -> Can not update another user");
        }
        InterviewEntity interviewToDelete = interviewRepository.findById(id);
        if(interviewToDelete==null){
            throw new InterviewDoesNotExistException("InterviewService -> Interview wasn't found by id: "+id+
                    " and can not be deleted.");
        }
        interviewRepository.deleteById(id);
        System.out.println("InterviewService -> Deleted interview: "+ interviewToDelete);
    }



}