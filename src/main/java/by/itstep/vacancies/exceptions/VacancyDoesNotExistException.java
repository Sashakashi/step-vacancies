package by.itstep.vacancies.exceptions;

public class VacancyDoesNotExistException extends RuntimeException{

    public VacancyDoesNotExistException(String message){
        super(message);
    }

}
