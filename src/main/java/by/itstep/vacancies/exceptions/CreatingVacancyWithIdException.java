package by.itstep.vacancies.exceptions;

public class CreatingVacancyWithIdException extends RuntimeException{

    public CreatingVacancyWithIdException(String message){
        super(message);
    }

}
