package by.itstep.vacancies.exceptions;

public class CreatingInterviewWithIdException extends RuntimeException{

    public CreatingInterviewWithIdException(String message){
        super(message);
    }

}
