package by.itstep.vacancies.exceptions;

public class CreatingUserWithIdException extends RuntimeException{

    public CreatingUserWithIdException(String message){
        super(message);
    }

}
