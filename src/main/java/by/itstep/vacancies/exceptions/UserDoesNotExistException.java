package by.itstep.vacancies.exceptions;

public class UserDoesNotExistException extends RuntimeException{

    public UserDoesNotExistException(String message){
        super(message);
    }

}
