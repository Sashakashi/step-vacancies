package by.itstep.vacancies.exceptions;

public class InterviewDoesNotExistException extends RuntimeException{

    public InterviewDoesNotExistException(String message){
        super(message);
    }

}
