package by.itstep.vacancies.exceptions;

public class UpdatingVacancyWithoutIdException extends RuntimeException{

    public UpdatingVacancyWithoutIdException(String message){
        super(message);
    }

}
