package by.itstep.vacancies.exceptions;

public class CreatingInterviewAlreadyExistsException extends RuntimeException{

    public CreatingInterviewAlreadyExistsException(String message){
        super(message);
    }

}
