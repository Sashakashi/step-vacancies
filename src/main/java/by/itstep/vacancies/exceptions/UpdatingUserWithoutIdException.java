package by.itstep.vacancies.exceptions;

public class UpdatingUserWithoutIdException extends RuntimeException{

    public UpdatingUserWithoutIdException(String message){
        super(message);
    }

}
