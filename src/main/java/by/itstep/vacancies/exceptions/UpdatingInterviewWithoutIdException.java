package by.itstep.vacancies.exceptions;

public class UpdatingInterviewWithoutIdException extends RuntimeException{

    public UpdatingInterviewWithoutIdException(String message){
        super(message);
    }

}
