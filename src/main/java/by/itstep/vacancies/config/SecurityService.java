package by.itstep.vacancies.config;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class SecurityService {
    @Autowired
    private UserRepository userRepository;

    public UserEntity getAuthenticatedUser(){
        String email = SecurityContextHolder.getContext().getAuthentication().getName();
        return userRepository.findByByEmail(email);
    }


}

