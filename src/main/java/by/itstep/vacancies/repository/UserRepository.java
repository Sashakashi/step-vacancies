package by.itstep.vacancies.repository;
import by.itstep.vacancies.entity.UserEntity;
import java.util.List;

public interface UserRepository {

    UserEntity findById(int id);
    List<UserEntity> findAll();
    UserEntity create(UserEntity entity);
    UserEntity update(UserEntity entity);
    void deleteById(int id);
    void deleteAll();
    UserEntity findByByEmail(String email);
}