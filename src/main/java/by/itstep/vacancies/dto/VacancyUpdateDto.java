package by.itstep.vacancies.dto;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class VacancyUpdateDto {
    @NotNull(message = "Id can not be null")
    private Integer id;
    @NotBlank(message = "Name can not be blank")
    @Size(min = 1, max = 100, message = "Name length must be between 1 and 100")
    private String name;
    @NotBlank(message = "Position can not be blank")
    @Size(min = 1, max = 100, message = "Position length must be between 1 and 100")
    private String position;
    @NotBlank(message = "Salary can not be blank")
    private Integer salary;
    @NotBlank(message = "CompanyName can not be blank")
    @Size(min = 1, max = 100, message = "CompanyName length must be between 1 and 100")
    private String companyName;
}
