package by.itstep.vacancies.dto;
import lombok.Data;

@Data
public class VacancyShortDto {

    private Integer id;
    private String name;
    private Integer salary;
    private String companyName;
}
