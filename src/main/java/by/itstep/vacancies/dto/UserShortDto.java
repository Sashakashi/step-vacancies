package by.itstep.vacancies.dto;
import lombok.Data;

@Data
public class UserShortDto {

    private Integer id;
    private String firstName;
    private String lastName;
    private Integer yearsOfExperience;
    private String position;
}
