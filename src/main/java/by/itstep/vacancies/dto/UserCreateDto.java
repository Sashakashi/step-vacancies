package by.itstep.vacancies.dto;
import lombok.Data;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

@Data
public class UserCreateDto {
    @NotBlank(message = "FirstName can not be blank")
    @Size(min = 1, max = 50, message = "FirstName length must be between 1 and 50")
    private String firstName;
    @NotBlank(message = "LastName can not be blank")
    @Size(min = 1, max = 50, message = "LastName length must be between 1 and 50")
    private String lastName;
    @NotBlank(message = "Phone can not be blank")
    @Size(min = 7, max = 20, message = "Phone length must be between 7 and 20")
    private String phone;
    private Integer yearsOfExperience;
    private String position;
    @Email(message = "Email must have valid format")
    @NotEmpty(message = "Email can not be null")
    private String email;
    @NotBlank(message = "Password can not be blank")
    @Size(min = 8, max = 100, message = "Password length must be between 8 and 100")
    private String password;
    private String role;
}
