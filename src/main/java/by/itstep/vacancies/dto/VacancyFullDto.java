package by.itstep.vacancies.dto;
import lombok.Data;

@Data
public class VacancyFullDto {

    private Integer id;
    private String name;
    private String position;
    private Integer salary;
    private String companyName;
}
