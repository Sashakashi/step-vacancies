package by.itstep.vacancies.dto;
import by.itstep.vacancies.entity.InterviewStatus;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.entity.VacancyEntity;
import lombok.Data;
import java.time.LocalDate;

@Data
public class InterviewFullDto {

    private Integer id;
    private InterviewStatus status;
    private LocalDate date;
    private UserEntity user;
    private VacancyEntity vacancy;



}
