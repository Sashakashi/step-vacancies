package by.itstep.vacancies.dto;
import by.itstep.vacancies.entity.InterviewStatus;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.entity.VacancyEntity;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class InterviewUpdateDto {
    @NotNull(message = "Id can not be null")
    private Integer id;
    @NotBlank(message = "Status can not be blank")
    private InterviewStatus status;
}
