package by.itstep.vacancies.dto;
import by.itstep.vacancies.entity.InterviewStatus;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.entity.VacancyEntity;
import lombok.Data;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class InterviewCreateDto {
    @NotNull(message = "UserId can not be null")
    private int userId;
    @NotNull(message = "VacancyId can not be null")
    private int vacancyId;
}
