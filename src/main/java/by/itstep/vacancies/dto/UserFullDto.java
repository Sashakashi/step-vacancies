package by.itstep.vacancies.dto;
import lombok.Data;

@Data
public class UserFullDto {

    private Integer id;
    private String firstName;
    private String lastName;
    private String phone;
    private Integer yearsOfExperience;
    private String position;
    private String email;
}
