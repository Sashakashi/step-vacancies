package by.itstep.vacancies.entity;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name="interviews")
@Data
public class InterviewEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name="status")
    @Enumerated(EnumType.STRING)
    private InterviewStatus status;

    @Column(name="date")
    private LocalDate date;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="user_id")
    private UserEntity user;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name="vacancy_id")
    private VacancyEntity vacancy;

}
