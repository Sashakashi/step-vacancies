package by.itstep.vacancies.entity;

public enum InterviewStatus {

    NEW, PROCESSED, REJECTED, FINISHED

}
