package by.itstep.vacancies.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name="vacancies")
@Data
public class VacancyEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name="name")
    private String name;

    @Column(name="position")
    private String position;

    @Column(name="salary")
    private Integer salary;

    @Column(name="company_name")
    private String companyName;

    @JsonIgnore
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    @OneToMany(mappedBy = "vacancy", fetch = FetchType.LAZY)
    private List<InterviewEntity> interviews;

}
