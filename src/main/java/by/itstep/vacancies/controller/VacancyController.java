package by.itstep.vacancies.controller;
import by.itstep.vacancies.dto.VacancyCreateDto;
import by.itstep.vacancies.dto.VacancyFullDto;
import by.itstep.vacancies.dto.VacancyShortDto;
import by.itstep.vacancies.dto.VacancyUpdateDto;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.entity.VacancyEntity;
import by.itstep.vacancies.service.VacancyService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@Controller
public class VacancyController {
    @Autowired
    private VacancyService vacancyService;

    @ResponseBody
    @RequestMapping(value = "/vacancies", method = RequestMethod.GET)
    public List<VacancyShortDto> findAllVacancies(){
        List<VacancyShortDto> allVacancies = vacancyService.findAll();
        return allVacancies;
    }

    @ResponseBody
    @RequestMapping(value = "/vacancies/{id}", method = RequestMethod.GET)
    public VacancyFullDto findById(@PathVariable int id){
        VacancyFullDto vacancy = vacancyService.findById(id);
        return vacancy;
    }

    @ResponseBody
    @RequestMapping(value = "/vacancies", method = RequestMethod.POST)
    public VacancyFullDto create(@Valid @RequestBody VacancyCreateDto createRequest){
        VacancyFullDto createdVacancy = vacancyService.create(createRequest);
        return createdVacancy;
    }


    @ResponseBody
    @RequestMapping(value = "/vacancies", method = RequestMethod.PUT)
    public VacancyFullDto update(@Valid @RequestBody VacancyUpdateDto updateRequest){
        VacancyFullDto updatedVacancy = vacancyService.update(updateRequest);
        return updatedVacancy;
    }

    @ResponseBody
    @RequestMapping(value = "/vacancies/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id){
        vacancyService.deleteById(id);
    }

}

