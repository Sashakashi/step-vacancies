package by.itstep.vacancies.controller;
import by.itstep.vacancies.dto.InterviewCreateDto;
import by.itstep.vacancies.dto.InterviewFullDto;
import by.itstep.vacancies.dto.InterviewUpdateDto;
import by.itstep.vacancies.entity.InterviewEntity;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.entity.VacancyEntity;
import by.itstep.vacancies.service.InterviewService;
import by.itstep.vacancies.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;
@Controller
public class InterviewController {
    @Autowired
    private InterviewService interviewService;

    @ResponseBody
    @RequestMapping(value = "/interviews", method = RequestMethod.GET)
    public List<InterviewFullDto> findAllInterviews(){
        List<InterviewFullDto> allInterviews = interviewService.findAll();
        return allInterviews;
    }

    @ResponseBody
    @RequestMapping(value = "/interviews/{id}", method = RequestMethod.GET)
    public InterviewFullDto findById(@PathVariable int id){
        InterviewFullDto interview = interviewService.findById(id);
        return interview;
    }

    @ResponseBody
    @RequestMapping(value = "/interviews", method = RequestMethod.POST)
    public InterviewFullDto create(@Valid @RequestBody InterviewCreateDto createRequest){
        InterviewFullDto createdInterview = interviewService.create(createRequest);
        return createdInterview;
    }


    @ResponseBody
    @RequestMapping(value = "/interviews", method = RequestMethod.PUT)
    public InterviewFullDto update(@Valid @RequestBody InterviewUpdateDto updateRequest){
        InterviewFullDto updatedInterview = interviewService.updateStatus(updateRequest);
        return updatedInterview;
    }


    @ResponseBody
    @RequestMapping(value = "/interviews/{id}", method = RequestMethod.DELETE)
    public void delete(@PathVariable int id){
        interviewService.deleteById(id);
    }

}
