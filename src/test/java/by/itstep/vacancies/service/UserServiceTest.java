package by.itstep.vacancies.service;
import by.itstep.vacancies.dto.UserCreateDto;
import by.itstep.vacancies.dto.UserFullDto;
import by.itstep.vacancies.dto.UserShortDto;
import by.itstep.vacancies.dto.UserUpdateDto;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.exceptions.CreatingUserWithIdException;
import by.itstep.vacancies.exceptions.EmailIsAlreadyTakenException;
import by.itstep.vacancies.exceptions.UpdatingUserWithoutIdException;
import by.itstep.vacancies.exceptions.UserDoesNotExistException;
import by.itstep.vacancies.mapper.UserMapper;
import by.itstep.vacancies.repository.UserRepository;
import by.itstep.vacancies.utils.DatabaseCleaner;
import by.itstep.vacancies.utils.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.util.List;

@SpringBootTest
public class UserServiceTest {

    @Autowired
    private DatabaseCleaner cleaner;
    @Autowired
    private UserService userService;
    @Autowired
    private UserMapper userMapper;

    @BeforeEach
    public void setUp(){
        cleaner.clean();
    }

//    @AfterEach                                                    unnecessary
//    public void clean(){
//        cleaner.clean();
//    }

    @Test
    public void create_happyPath(){
        //given
        UserCreateDto userToCreate = EntityUtils.prepareUser();
        //when
        UserFullDto createdUser = userService.create(userToCreate);
        //then
        Assertions.assertNotNull(createdUser.getId());
        UserFullDto foundUser = userService.findById(createdUser.getId());
        Assertions.assertEquals(userToCreate.getEmail(),foundUser.getEmail());
        Assertions.assertEquals(userToCreate.getFirstName(),foundUser.getFirstName());
        Assertions.assertEquals(userToCreate.getLastName(),foundUser.getLastName());
        Assertions.assertEquals(userToCreate.getPhone(),foundUser.getPhone());
        Assertions.assertEquals(userToCreate.getPosition(),foundUser.getPosition());
        Assertions.assertEquals(userToCreate.getYearsOfExperience(),foundUser.getYearsOfExperience());
    }

//    @Test
//    public void create_whenCreatingWithId(){                          impossible, unnecessary
//        //given
//        UserEntity userToCreate = EntityUtils.prepareUser();
//        userToCreate.setId(1000);
//        //when
//        //then
//        Assertions.assertThrows(CreatingUserWithIdException.class, ()->userService.create(userToCreate));
//    }

    @Test
    public void create_whenEmailIsAlreadyTaken(){
        //given
        UserFullDto user1 = addUserToDB();
        UserCreateDto user2 = EntityUtils.prepareUser();
        user2.setEmail(user1.getEmail());
        //when
        //then
        Assertions.assertThrows(EmailIsAlreadyTakenException.class, ()->userService.create(user2));
    }

    @Test
    public void findById_happyPath(){
        //given
        UserFullDto userToFind = addUserToDB();
        //when
        UserFullDto foundUser = userService.findById(userToFind.getId());
        //then
        Assertions.assertEquals(userToFind.getId(),foundUser.getId());
        Assertions.assertEquals(userToFind.getFirstName(),foundUser.getFirstName());
        Assertions.assertEquals(userToFind.getLastName(),foundUser.getLastName());
        Assertions.assertEquals(userToFind.getEmail(),foundUser.getEmail());
        Assertions.assertEquals(userToFind.getPosition(),foundUser.getPosition());
        Assertions.assertEquals(userToFind.getPhone(),foundUser.getPhone());
        Assertions.assertEquals(userToFind.getYearsOfExperience(),foundUser.getYearsOfExperience());
    }

    @Test
    public void findById_whenNoOneFound(){
        //given
        //when
        //then
        Assertions.assertThrows(UserDoesNotExistException.class,()->userService.findById(10));
    }

    @Test
    public void findAll_happyPath(){
        //given
        addUserToDB();
        addUserToDB();
        //when
        List<UserShortDto> foundUsers = userService.findAll();
        //then
        Assertions.assertEquals(2, foundUsers.size());
    }

    @Test
    public void findAll_whenNoOneFound(){
        //given
        //when
        List<UserShortDto> foundUsers = userService.findAll();
        //then
        Assertions.assertTrue(foundUsers.isEmpty());
    }


    @Test
    public void update_happyPath(){
        //given
        UserFullDto existingUser = addUserToDB();
        UserUpdateDto updateDto = new UserUpdateDto();
        updateDto.setFirstName("updated");
        updateDto.setLastName("updated");
        updateDto.setPosition("updated");
        updateDto.setPhone("updated");
        updateDto.setId(existingUser.getId());
        updateDto.setYearsOfExperience(15);
        //when
        UserFullDto updatedUser = userService.update(updateDto);
        //then
        Assertions.assertEquals(existingUser.getId(), updatedUser.getId());
        UserFullDto foundUser = userService.findById(existingUser.getId());
        Assertions.assertEquals(updateDto.getFirstName(),foundUser.getFirstName());
        Assertions.assertEquals(updateDto.getLastName(),foundUser.getLastName());
        Assertions.assertEquals(updateDto.getPosition(),foundUser.getPosition());
        Assertions.assertEquals(updateDto.getPhone(),foundUser.getPhone());
        Assertions.assertEquals(updateDto.getYearsOfExperience(),foundUser.getYearsOfExperience());
    }

    @Test
    public void update_WhenUpdatingWithoutId(){
        //given
        UserUpdateDto updateDto = new UserUpdateDto();
        updateDto.setId(null);
        updateDto.setFirstName("updated");
        updateDto.setLastName("updated");
        updateDto.setPosition("updated");
        updateDto.setPhone("updated");
        updateDto.setYearsOfExperience(15);
        //when
        //then
        Assertions.assertThrows(UpdatingUserWithoutIdException.class,()->userService.update(updateDto));
    }


    @Test
    public void delete_happyPath(){
        //given
        UserFullDto existingUser = addUserToDB();
        //when
        userService.deleteById(existingUser.getId());
        //then
        Assertions.assertThrows(UserDoesNotExistException.class, () ->  userService.findById(existingUser.getId()));
    }

    @Test
    public void delete_whenThereIsNoSuchUser(){
        //given
        //when
        //then
        Assertions.assertThrows(UserDoesNotExistException.class, () ->  userService.deleteById(10));
    }

    private UserFullDto addUserToDB(){
        UserCreateDto userDtoToAdd = EntityUtils.prepareUser();
        return userService.create(userDtoToAdd);
    }

}
