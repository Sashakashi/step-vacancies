package by.itstep.vacancies.service;
import by.itstep.vacancies.dto.VacancyCreateDto;
import by.itstep.vacancies.dto.VacancyFullDto;
import by.itstep.vacancies.dto.VacancyShortDto;
import by.itstep.vacancies.dto.VacancyUpdateDto;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.entity.VacancyEntity;
import by.itstep.vacancies.exceptions.*;
import by.itstep.vacancies.utils.DatabaseCleaner;
import by.itstep.vacancies.utils.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
public class VacancyServiceTest {

    @Autowired
    private VacancyService vacancyService;
    @Autowired
    private DatabaseCleaner cleaner;

    @BeforeEach
    public void setUp(){
        cleaner.clean();
    }

//    @AfterEach
//    public void clean(){
//        cleaner.clean();
//    }



    @Test
    public void create_happyPath(){
        //given
        VacancyCreateDto vacancyToCreate = EntityUtils.prepareVacancy();
        //when
        VacancyFullDto createdVacancy = vacancyService.create(vacancyToCreate);
        //then
        Assertions.assertNotNull(createdVacancy.getId());
        VacancyFullDto foundVacancy = vacancyService.findById(createdVacancy.getId());
        Assertions.assertEquals(vacancyToCreate.getName(),foundVacancy.getName());
        Assertions.assertEquals(vacancyToCreate.getPosition(),foundVacancy.getPosition());
        Assertions.assertEquals(vacancyToCreate.getCompanyName(),foundVacancy.getCompanyName());
        Assertions.assertEquals(vacancyToCreate.getSalary(),foundVacancy.getSalary());
    }

//    @Test                                                                               //unnecessary
//    public void create_whenCreatingWithId(){
//        //given
//        VacancyEntity vacancyToCreate = EntityUtils.prepareVacancy();
//        vacancyToCreate.setId(1000);
//        //when
//        //then
//        Assertions.assertThrows(CreatingVacancyWithIdException.class, ()->vacancyService.create(vacancyToCreate));
//    }

    @Test
    public void findById_happyPath() {
        //given
        VacancyFullDto vacancyToFind = addVacancyToDB();
        //when
        VacancyFullDto foundVacancy = vacancyService.findById(vacancyToFind.getId());
        //then
        Assertions.assertEquals(vacancyToFind.getId(), foundVacancy.getId());
        Assertions.assertEquals(vacancyToFind.getName(), foundVacancy.getName());
        Assertions.assertEquals(vacancyToFind.getCompanyName(), foundVacancy.getCompanyName());
        Assertions.assertEquals(vacancyToFind.getPosition(), foundVacancy.getPosition());
        Assertions.assertEquals(vacancyToFind.getSalary(), foundVacancy.getSalary());
    }

    @Test
    public void findById_whenNoOneFound(){
        //given
        //when
        //then
        Assertions.assertThrows(VacancyDoesNotExistException.class,()->vacancyService.findById(10));
    }

    @Test
    public void findAll_happyPath(){
        //given
        addVacancyToDB();
        addVacancyToDB();
        //when
        List<VacancyShortDto> foundVacancies = vacancyService.findAll();
        //then
        Assertions.assertEquals(2, foundVacancies.size());
    }

    @Test
    public void findAll_whenNoOneFound(){
        //given
        //when
        List<VacancyShortDto> foundVacancies = vacancyService.findAll();
        //then
        Assertions.assertTrue(foundVacancies.isEmpty());
    }

    @Test
    public void update_happyPath(){
        //given
        VacancyFullDto existingVacancy = addVacancyToDB();
        VacancyUpdateDto updateRequest = new VacancyUpdateDto();
        updateRequest.setId(existingVacancy.getId());
        updateRequest.setName("updated");
        updateRequest.setCompanyName("updated");
        updateRequest.setPosition("updated");
        updateRequest.setSalary(100);
        //when
        VacancyFullDto updatedVacancy = vacancyService.update(updateRequest);
        //then
        Assertions.assertEquals(existingVacancy.getId(), updatedVacancy.getId());
        VacancyFullDto foundVacancy = vacancyService.findById(existingVacancy.getId());
        Assertions.assertEquals(updateRequest.getName(),foundVacancy.getName());
        Assertions.assertEquals(updateRequest.getCompanyName(),foundVacancy.getCompanyName());
        Assertions.assertEquals(updateRequest.getPosition(),foundVacancy.getPosition());
        Assertions.assertEquals(updateRequest.getSalary(),foundVacancy.getSalary());
    }

    @Test
    public void update_WhenUpdatingWithoutId(){
        //given
        VacancyFullDto existingVacancy = addVacancyToDB();
        VacancyUpdateDto updateRequest = new VacancyUpdateDto();
        updateRequest.setId(null);
        updateRequest.setName("updated");
        updateRequest.setCompanyName("updated");
        updateRequest.setPosition("updated");
        updateRequest.setSalary(100);
        //when
        //then
        Assertions.assertThrows(UpdatingVacancyWithoutIdException.class,()->vacancyService.update(updateRequest));
    }

    @Test
    public void delete_happyPath(){
        //given
        VacancyFullDto existingVacancy = addVacancyToDB();
        //when
        vacancyService.deleteById(existingVacancy.getId());
        //then
        Assertions.assertThrows(VacancyDoesNotExistException.class, () ->  vacancyService.findById(existingVacancy.getId()));
    }

    @Test
    public void delete_whenThereIsNoSuchUser(){
        //given
        //when
        //then
        Assertions.assertThrows(VacancyDoesNotExistException.class, () ->  vacancyService.deleteById(10));
    }


    private VacancyFullDto addVacancyToDB(){
        VacancyCreateDto vacancyToAdd = EntityUtils.prepareVacancy();
        return vacancyService.create(vacancyToAdd);
    }
}
