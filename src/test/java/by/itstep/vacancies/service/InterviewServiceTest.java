package by.itstep.vacancies.service;
import by.itstep.vacancies.dto.*;
import by.itstep.vacancies.entity.InterviewEntity;
import by.itstep.vacancies.entity.InterviewStatus;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.entity.VacancyEntity;
import by.itstep.vacancies.exceptions.*;
import by.itstep.vacancies.utils.DatabaseCleaner;
import by.itstep.vacancies.utils.EntityUtils;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import java.time.LocalDate;
import java.util.List;

import static by.itstep.vacancies.entity.InterviewStatus.FINISHED;
import static by.itstep.vacancies.entity.InterviewStatus.NEW;

@SpringBootTest
public class InterviewServiceTest {

    @Autowired
    private VacancyService vacancyService;
    @Autowired
    private UserService userService;
    @Autowired
    private InterviewService interviewService;
    @Autowired
    private DatabaseCleaner cleaner;

    @BeforeEach
    public void setUp(){
        cleaner.clean();
    }

//    @AfterEach
//    public void clean(){
//        cleaner.clean();
//    }

    @Test
    public void create_happyPath(){
        //given
        //when
        InterviewFullDto createdInterview = addInterviewToDB();
        //then
        Assertions.assertNotNull(createdInterview.getId());
        InterviewFullDto foundInterview = interviewService.findById(createdInterview.getId());
        Assertions.assertEquals(createdInterview.getDate(),foundInterview.getDate());
        Assertions.assertEquals(createdInterview.getStatus(),foundInterview.getStatus());
        Assertions.assertEquals(createdInterview.getUser().getId(),foundInterview.getUser().getId());
        Assertions.assertEquals(createdInterview.getVacancy().getId(),foundInterview.getVacancy().getId());
    }

    @Test
    public void create_whenAppointingSecondTime(){
        //given
        UserFullDto user = userService.create(EntityUtils.prepareUser());
        VacancyFullDto vacancy = vacancyService.create(EntityUtils.prepareVacancy());
        InterviewCreateDto dtoToCreate = new InterviewCreateDto();
        dtoToCreate.setUserId(user.getId());
        dtoToCreate.setVacancyId(vacancy.getId());
        interviewService.create(dtoToCreate);
        //when
        //then
        Assertions.assertThrows(CreatingInterviewAlreadyExistsException.class,
                ()->interviewService.create(dtoToCreate));
    }

    @Test
    public void findById_happyPath() {
        //given
        InterviewFullDto interviewToFind = addInterviewToDB();
        //when
        InterviewFullDto foundInterview = interviewService.findById(interviewToFind.getId());
        //then
        Assertions.assertEquals(interviewToFind.getId(), foundInterview.getId());
        Assertions.assertEquals(interviewToFind.getStatus(), foundInterview.getStatus());
        Assertions.assertEquals(interviewToFind.getDate(), foundInterview.getDate());
        Assertions.assertEquals(interviewToFind.getUser().getId(), foundInterview.getUser().getId());
        Assertions.assertEquals(interviewToFind.getVacancy().getId(), foundInterview.getVacancy().getId());
    }

    @Test
    public void findById_whenNoOneFound(){
        //given
        //when
        //then
        Assertions.assertThrows(InterviewDoesNotExistException.class,()->interviewService.findById(10));
    }

    @Test
    public void findAll_happyPath(){
        //given
        addInterviewToDB();
        addInterviewToDB();
        //when
        List<InterviewFullDto> foundInterviews = interviewService.findAll();
        //then
        Assertions.assertEquals(2, foundInterviews.size());
    }

    @Test
    public void findAll_whenNoOneFound(){
        //given
        //when
        List<InterviewFullDto> foundInterviews = interviewService.findAll();
        //then
        Assertions.assertTrue(foundInterviews.isEmpty());
    }

    @Test
    public void updateStatus_happyPath(){
        //given
        InterviewFullDto existingInterview = addInterviewToDB();
        InterviewUpdateDto dtoToUpdate = new InterviewUpdateDto();
//        UserFullDto anotherUser = userService.create(EntityUtils.prepareUser());
//        VacancyFullDto anotherVacancy = vacancyService.create(EntityUtils.prepareVacancy());
//        existingInterview.setDate(LocalDate.now().plusDays(10));
        dtoToUpdate.setId(existingInterview.getId());
        dtoToUpdate.setStatus(FINISHED);
//        existingInterview.setUser(anotherUser);
//        existingInterview.setVacancy(anotherVacancy);
        //when
        InterviewFullDto updatedInterview = interviewService.updateStatus(dtoToUpdate);
        //then
        Assertions.assertEquals(existingInterview.getId(), updatedInterview.getId());
        InterviewFullDto foundInterview = interviewService.findById(existingInterview.getId());
        Assertions.assertEquals(dtoToUpdate.getStatus(),foundInterview.getStatus());
    }

    @Test
    public void updateStatus_whenUpdatingWithoutId(){
        //given
        InterviewUpdateDto updateRequest = new InterviewUpdateDto();
        updateRequest.setId(null);
        updateRequest.setStatus(FINISHED);
        //when
        //then
        Assertions.assertThrows(UpdatingInterviewWithoutIdException.class,()->interviewService.updateStatus(updateRequest));
    }


    @Test
    public void delete_happyPath(){
        //given
        InterviewFullDto existingInterview = addInterviewToDB();
        //when
        interviewService.deleteById(existingInterview.getId());
        //then
        Assertions.assertThrows(InterviewDoesNotExistException.class, () ->  interviewService.findById(existingInterview.getId()));
    }

    @Test
    public void delete_whenThereIsNoSuchUser(){
        //given
        //when
        //then
        Assertions.assertThrows(InterviewDoesNotExistException.class, () ->  interviewService.deleteById(10));
    }



    private InterviewFullDto addInterviewToDB(){
        UserFullDto user = userService.create(EntityUtils.prepareUser());
        VacancyFullDto vacancy = vacancyService.create(EntityUtils.prepareVacancy());
        InterviewFullDto interview = interviewService.create(EntityUtils.prepareInterview(user.getId(),vacancy.getId()));
        return interview;
    }

}
