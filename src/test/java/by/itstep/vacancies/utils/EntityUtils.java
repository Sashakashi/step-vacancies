package by.itstep.vacancies.utils;
import by.itstep.vacancies.dto.InterviewCreateDto;
import by.itstep.vacancies.dto.UserCreateDto;
import by.itstep.vacancies.dto.VacancyCreateDto;
import by.itstep.vacancies.entity.InterviewEntity;
import by.itstep.vacancies.entity.UserEntity;
import by.itstep.vacancies.entity.VacancyEntity;
import by.itstep.vacancies.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.time.LocalDate;

import static by.itstep.vacancies.entity.InterviewStatus.NEW;

@Component
public class EntityUtils {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private VacancyRepository vacancyRepository;

    public static UserCreateDto prepareUser(){
        UserCreateDto user = new UserCreateDto();
        user.setFirstName("Firstname#"+Math.random());
        user.setLastName("Lastname#"+Math.random());
        user.setPhone("123456");
        user.setRole("Role#"+Math.random());
        user.setPosition("Position#"+Math.random());
        user.setPassword("Pass"+Math.random());
        user.setEmail(Math.random()+"@gmail.com");
        user.setYearsOfExperience((int)(Math.random()*10));
        return user;
    }

    public static VacancyCreateDto prepareVacancy(){
        VacancyCreateDto vacancy = new VacancyCreateDto();
        vacancy.setName("Name#"+Math.random());
        vacancy.setPosition("Position#"+Math.random());
        vacancy.setCompanyName("Companyname#"+Math.random());
        vacancy.setSalary((int)(Math.random()*10));
        return vacancy;
    }

    public static InterviewCreateDto prepareInterview(int userId, int vacancyId){
        InterviewCreateDto interview = new InterviewCreateDto();
        interview.setUserId(userId);
        interview.setVacancyId(vacancyId);
        return interview;
    }

//    public static InterviewEntity prepareInterview(){
//        UserRepository userRepository = ServiceFactory.getUserRepository();
//        VacancyRepository vacancyRepository = ServiceFactory.getVacancyRepository();
//        UserEntity user = prepareUser();
//        VacancyEntity vacancy = prepareVacancy();
//        userRepository.create(user);
//        vacancyRepository.create(vacancy);
//        InterviewEntity interview = new InterviewEntity();
//        interview.setStatus("Status#"+Math.random());
//        interview.setDate(LocalDate.now().plusDays(2));
//        interview.setUser(user);
//        interview.setVacancy(vacancy);
//        return interview;
//    }


}
