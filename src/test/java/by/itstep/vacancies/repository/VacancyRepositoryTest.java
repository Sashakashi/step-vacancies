//package by.itstep.vacancies.repository;
//import by.itstep.vacancies.entity.UserEntity;
//import by.itstep.vacancies.entity.VacancyEntity;
//import by.itstep.vacancies.utils.DatabaseCleaner;
//import by.itstep.vacancies.utils.EntityUtils;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.util.List;
//
//@SpringBootTest
//public class VacancyRepositoryTest {
//
//    @Autowired
//    private VacancyRepository vacancyRepository;
//    @Autowired
//    private DatabaseCleaner cleaner;
//
//    @BeforeEach
//    public void setUp(){
//        cleaner.clean();
//    }
//
//    @AfterEach
//    public void clean(){
//        cleaner.clean();
//    }
//
//    @Test
//    public void create_happyPath(){
//        //given
//        VacancyEntity vacancyToCreate = EntityUtils.prepareVacancy();
//        //when
//        VacancyEntity createdVacancy = vacancyRepository.create(vacancyToCreate);
//        //then
//        Assertions.assertNotNull(createdVacancy.getId());
//        VacancyEntity foundVacancy = vacancyRepository.findById(createdVacancy.getId());
//        Assertions.assertEquals(vacancyToCreate.getPosition(),foundVacancy.getPosition());
//        Assertions.assertEquals(vacancyToCreate.getCompanyName(),foundVacancy.getCompanyName());
//        Assertions.assertEquals(vacancyToCreate.getName(),foundVacancy.getName());
//        Assertions.assertEquals(vacancyToCreate.getSalary(),foundVacancy.getSalary());
//    }
//
//    @Test
//    public void findAll_happyPath(){
//        //given
//        addVacancyToDB();
//        addVacancyToDB();
//        //when
//        List<VacancyEntity> foundVacancies = vacancyRepository.findAll();
//        //then
//        Assertions.assertEquals(2, foundVacancies.size());
//    }
//
//    @Test
//    public void findAll_whenNoOneFound(){
//        //given
//        //when
//        List<VacancyEntity> foundVacancies = vacancyRepository.findAll();
//        //then
//        Assertions.assertTrue(foundVacancies.isEmpty());
//    }
//
//    @Test
//    public void update_happyPath(){
//        //given
//        VacancyEntity existingVacancy = addVacancyToDB();
//        existingVacancy.setName("updated");
//        existingVacancy.setPosition("updated");
//        existingVacancy.setCompanyName("updated");
//        existingVacancy.setSalary(1000);
//        //when
//        VacancyEntity updatedVacancy = vacancyRepository.update(existingVacancy);
//        //then
//        Assertions.assertEquals(existingVacancy.getId(), updatedVacancy.getId());
//        VacancyEntity foundVacancy = vacancyRepository.findById(existingVacancy.getId());
//        Assertions.assertEquals(existingVacancy.getName(),foundVacancy.getName());
//        Assertions.assertEquals(existingVacancy.getCompanyName(),foundVacancy.getCompanyName());
//        Assertions.assertEquals(existingVacancy.getPosition(),foundVacancy.getPosition());
//        Assertions.assertEquals(existingVacancy.getSalary(),foundVacancy.getSalary());
//    }
//
//    @Test
//    public void delete_happyPath(){
//        //given
//        VacancyEntity existingVacancy = addVacancyToDB();
//        //when
//        vacancyRepository.deleteById(existingVacancy.getId());
//        //then
//        VacancyEntity foundVacancy = vacancyRepository.findById(existingVacancy.getId());
//        Assertions.assertNull(foundVacancy);
//    }
//
//
//    private VacancyEntity addVacancyToDB(){
//        VacancyEntity vacancyToAdd = EntityUtils.prepareVacancy();
//        return vacancyRepository.create(vacancyToAdd);
//    }
//
//}
