//package by.itstep.vacancies.repository;
//import by.itstep.vacancies.entity.UserEntity;
//import by.itstep.vacancies.utils.DatabaseCleaner;
//import by.itstep.vacancies.utils.EntityUtils;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//import java.util.List;
//
//@SpringBootTest
//public class UserRepositoryTest {
//
//    @Autowired
//    private UserRepository userRepository;
//    @Autowired
//    private DatabaseCleaner cleaner;
//
//    @BeforeEach
//    public void setUp(){
//        cleaner.clean();
//    }
//
//    @AfterEach
//    public void clean(){
//        cleaner.clean();
//    }
//
//    @Test
//    public void create_happyPath(){
//        //given
//        UserEntity userToCreate = EntityUtils.prepareUser();
//        //when
//        UserEntity createdUser = userRepository.create(userToCreate);
//        //then
//        Assertions.assertNotNull(createdUser.getId());
//        UserEntity foundUser = userRepository.findById(createdUser.getId());
//        Assertions.assertEquals(userToCreate.getEmail(),foundUser.getEmail());
//        Assertions.assertEquals(userToCreate.getFirstName(),foundUser.getFirstName());
//        Assertions.assertEquals(userToCreate.getLastName(),foundUser.getLastName());
//        Assertions.assertEquals(userToCreate.getPassword(),foundUser.getPassword());
//        Assertions.assertEquals(userToCreate.getPhone(),foundUser.getPhone());
//        Assertions.assertEquals(userToCreate.getPosition(),foundUser.getPosition());
//        Assertions.assertEquals(userToCreate.getRole(),foundUser.getRole());
//        Assertions.assertEquals(userToCreate.getYearsOfExperience(),foundUser.getYearsOfExperience());
//    }
//
//    @Test
//    public void findAll_happyPath(){
//        //given
//        addUserToDB();
//        addUserToDB();
//        //when
//        List<UserEntity> foundUsers = userRepository.findAll();
//        //then
//        Assertions.assertEquals(2, foundUsers.size());
//    }
//
//    @Test
//    public void findAll_whenNoOneFound(){
//        //given
//        //when
//        List<UserEntity> foundUsers = userRepository.findAll();
//        //then
//        Assertions.assertTrue(foundUsers.isEmpty());
//    }
//
//    @Test
//    public void update_happyPath(){
//        //given
//        UserEntity existingUser = addUserToDB();
//        existingUser.setFirstName("updated");
//        existingUser.setLastName("updated");
//        existingUser.setPosition("updated");
//        existingUser.setPhone("updated");
//        existingUser.setRole("updated");
//        existingUser.setEmail("updated");
//        existingUser.setPassword("updated");
//        existingUser.setYearsOfExperience(15);
//        //when
//        UserEntity updatedUser = userRepository.update(existingUser);
//        //then
//        Assertions.assertEquals(existingUser.getId(), updatedUser.getId());
//        UserEntity foundUser = userRepository.findById(existingUser.getId());
//        Assertions.assertEquals(existingUser.getFirstName(),foundUser.getFirstName());
//        Assertions.assertEquals(existingUser.getLastName(),foundUser.getLastName());
//        Assertions.assertEquals(existingUser.getPosition(),foundUser.getPosition());
//        Assertions.assertEquals(existingUser.getPhone(),foundUser.getPhone());
//        Assertions.assertEquals(existingUser.getPassword(),foundUser.getPassword());
//        Assertions.assertEquals(existingUser.getEmail(),foundUser.getEmail());
//        Assertions.assertEquals(existingUser.getRole(),foundUser.getRole());
//        Assertions.assertEquals(existingUser.getYearsOfExperience(),foundUser.getYearsOfExperience());
//    }
//
//    @Test
//    public void delete_happyPath(){
//        //given
//        UserEntity existingUser = addUserToDB();
//        //when
//        userRepository.deleteById(existingUser.getId());
//        //then
//        UserEntity foundUser = userRepository.findById(existingUser.getId());
//        Assertions.assertNull(foundUser);
//    }
//
//
//
//    private UserEntity addUserToDB(){
//        UserEntity userToAdd = EntityUtils.prepareUser();
//        return userRepository.create(userToAdd);
//    }
//
//}
