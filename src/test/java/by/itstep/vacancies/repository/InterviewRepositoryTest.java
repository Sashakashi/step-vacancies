//package by.itstep.vacancies.repository;
//import by.itstep.vacancies.entity.InterviewEntity;
//import by.itstep.vacancies.entity.UserEntity;
//import by.itstep.vacancies.entity.VacancyEntity;
//import by.itstep.vacancies.utils.DatabaseCleaner;
//import by.itstep.vacancies.utils.EntityUtils;
//import org.junit.jupiter.api.AfterEach;
//import org.junit.jupiter.api.Assertions;
//import org.junit.jupiter.api.BeforeEach;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import java.time.LocalDate;
//import java.util.List;
//
//import static by.itstep.vacancies.entity.InterviewStatus.NEW;
//
//@SpringBootTest
//public class InterviewRepositoryTest {
//
//    @Autowired
//    private UserRepository userRepository;
//    @Autowired
//    private VacancyRepository vacancyRepository;
//    @Autowired
//    private InterviewRepository interviewRepository;
//    @Autowired
//    private DatabaseCleaner cleaner;
//
//    @BeforeEach
//    public void setUp(){
//        cleaner.clean();
//    }
//
//    @AfterEach
//    public void clean(){
//        cleaner.clean();
//    }
//
//    @Test
//    public void create_happy_Path(){
//        //given
//        UserEntity user = EntityUtils.prepareUser();
//        userRepository.create(user);
//        VacancyEntity vacancy = EntityUtils.prepareVacancy();
//        vacancyRepository.create(vacancy);
//        InterviewEntity interviewToCreate = EntityUtils.prepareInterview(user,vacancy);
//        //when
//        InterviewEntity createdInterview = interviewRepository.create(interviewToCreate);
//        //then
//        Assertions.assertNotNull(createdInterview.getId());
//        InterviewEntity foundInterview = interviewRepository.findById(createdInterview.getId());
//        Assertions.assertEquals(interviewToCreate.getDate(),foundInterview.getDate());
//        Assertions.assertEquals(interviewToCreate.getStatus(),foundInterview.getStatus());
//        Assertions.assertEquals(interviewToCreate.getUser().getId(),foundInterview.getUser().getId());
//        Assertions.assertEquals(interviewToCreate.getVacancy().getId(),foundInterview.getVacancy().getId());
//    }
//
//    @Test
//    public void findAll_happyPath(){
//        //given
//        addInterviewToDB();
//        addInterviewToDB();
//        //when
//        List<InterviewEntity> foundInterviews = interviewRepository.findAll();
//        //then
//        Assertions.assertEquals(2, foundInterviews.size());
//    }
//
//    @Test
//    public void findAll_whenNoOneFound(){
//        //given
//        //when
//        List<InterviewEntity> foundInterviews = interviewRepository.findAll();
//        //then
//        Assertions.assertTrue(foundInterviews.isEmpty());
//    }
//
//    @Test
//    public void update_happyPath(){
//        //given
//        InterviewEntity existingInterview = addInterviewToDB();
//        UserEntity userToUpdate = EntityUtils.prepareUser();
//        userRepository.create(userToUpdate);
//        VacancyEntity vacancyToUpdate = EntityUtils.prepareVacancy();
//        vacancyRepository.create(vacancyToUpdate);
//        existingInterview.setStatus(NEW);
//        existingInterview.setDate(LocalDate.now().plusDays(5));
//        existingInterview.setVacancy(vacancyToUpdate);
//        existingInterview.setUser(userToUpdate);
//        //when
//        InterviewEntity updatedInterview = interviewRepository.update(existingInterview);
//        //then
//        Assertions.assertEquals(existingInterview.getId(), updatedInterview.getId());
//        InterviewEntity foundInterview = interviewRepository.findById(existingInterview.getId());
//        Assertions.assertEquals(existingInterview.getStatus(),foundInterview.getStatus());
//        Assertions.assertEquals(existingInterview.getDate(),foundInterview.getDate());
//        Assertions.assertEquals(existingInterview.getUser().getId(),foundInterview.getUser().getId());
//        Assertions.assertEquals(existingInterview.getVacancy().getId(),foundInterview.getVacancy().getId());
//    }
//
//    @Test
//    public void delete_happyPath(){
//        //given
//        InterviewEntity existingInterview = addInterviewToDB();
//        //when
//        interviewRepository.deleteById(existingInterview.getId());
//        //then
//        InterviewEntity foundInterview = interviewRepository.findById(existingInterview.getId());
//        Assertions.assertNull(foundInterview);
//    }
//
//    private InterviewEntity addInterviewToDB(){
//        UserEntity user = EntityUtils.prepareUser();
//        userRepository.create(user);
//        VacancyEntity vacancy = EntityUtils.prepareVacancy();
//        vacancyRepository.create(vacancy);
//        InterviewEntity interviewToAdd = EntityUtils.prepareInterview(user,vacancy);
//        return  interviewRepository.create(interviewToAdd);
//    }
//
//}
